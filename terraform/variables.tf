variable "project" {
  type    = string
  default = "installfest"
}

variable "project_name" {
  type    = string
  default = "installfest"
}

variable "region" {
  type    = string
  default = "us-central1"
}

variable "zone" {
  type    = string
  default = "us-central1-a"
}

variable "environment" {
  type = string
}
variable "disk_data_size" {
  type    = number
  default = 10
}

variable "disk_datas" {
  type    = list(string)
  default = []
}

variable "disk_system_size" {
  type    = number
  default = 15
}

variable "instance_size" {
  type    = string
  default = "e2-small"
}

variable "instances" {
  type    = list(string)
  default = []
}

variable "dns_zone" {
  type    = string
  default = "installfest.sgf.xyz."
}

variable "manage_ssh_keys" {
  type    = bool
  default = true
}
