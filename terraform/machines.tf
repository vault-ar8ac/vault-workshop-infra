module "machines" {
  for_each = toset(var.instances)
  source   = "./modules/machine"

  name = each.key

  project      = var.project
  project_name = var.project_name
  environment  = var.environment
  region       = var.region
  zone         = var.zone

  disk_data_size   = var.disk_data_size
  disk_datas       = var.disk_datas
  disk_system_size = var.disk_system_size
  instance_size    = var.instance_size
  instance_image   = "debian-cloud/debian-11"
}
