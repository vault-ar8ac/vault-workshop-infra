locals {
  name_slug = replace(replace(lower(var.name), "/\\W|_|\\s/", "-"), "/-+/", "-")
}

resource "google_compute_address" "server_public_ip" {
  address_type = "EXTERNAL"
  name         = "pip-${var.project_name}-${var.environment}-${local.name_slug}"
  network_tier = "STANDARD"
}

resource "google_service_account" "server_service_account" {
  account_id = "vm-${var.project_name}-${var.environment}-${local.name_slug}"
}

resource "google_compute_disk" "server_data" {
  for_each = toset(var.disk_datas)

  name = "stg-${var.project_name}-${var.environment}-${local.name_slug}-data-${each.key}"
  size = var.disk_data_size
  type = "pd-standard"
}

resource "google_compute_instance" "server_instance" {
  deletion_protection       = false
  can_ip_forward            = false
  enable_display            = false
  labels                    = {}
  machine_type              = var.instance_size
  name                      = "vm-${var.project_name}-${var.environment}-${local.name_slug}"
  allow_stopping_for_update = true
  tags                      = var.network_tags
  boot_disk {
    auto_delete = true

    initialize_params {
      image  = var.instance_image
      labels = {}
      size   = var.disk_system_size
      type   = "pd-standard"
    }
  }

  network_interface {
    network            = "https://www.googleapis.com/compute/v1/projects/${var.project}/global/networks/default"
    subnetwork         = "https://www.googleapis.com/compute/v1/projects/${var.project}/regions/${var.region}/subnetworks/default"
    subnetwork_project = var.project

    access_config {
      nat_ip       = google_compute_address.server_public_ip.address
      network_tier = "STANDARD"
    }
  }

  scheduling {
    automatic_restart   = true
    on_host_maintenance = "MIGRATE"
    preemptible         = false
  }

  service_account {
    email = google_service_account.server_service_account.email
    scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
    ]
  }

  lifecycle {
    ignore_changes = [attached_disk]
  }
}

resource "google_compute_attached_disk" "data_disks" {
  for_each = toset(var.disk_datas)

  disk     = google_compute_disk.server_data[each.key].id
  instance = google_compute_instance.server_instance.id
}