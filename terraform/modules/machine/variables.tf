variable "project" {
  type = string
}

variable "project_name" {
  type = string
}

variable "region" {
  type = string
}

variable "zone" {
  type = string
}

variable "name" {
  type = string
}

variable "environment" {
  type = string
}

variable "disk_datas" {
  type    = list(string)
  default = []
}

variable "disk_data_size" {
  type = number
}

variable "disk_system_size" {
  type = number
}

variable "instance_size" {
  type = string
}

variable "network_tags" {
  type    = list(string)
  default = ["http-server", "https-server"]
}

variable "instance_image" {
  type    = string
  default = "debian-cloud/debian-11"
}