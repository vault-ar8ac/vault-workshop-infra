resource "google_compute_project_metadata" "metadata" {
  count   = var.manage_ssh_keys ? 1 : 0
  project = var.project
  metadata = {
    "ssh-keys" = <<-EOT
            jindrich.skupa:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDTCXxZ0kcgXZ9pHx0RnBNsdyM6ajCPCebew4dHfJOtSmcRYuZnKXRqdhW+usGC9itdxx3hxZr7JaCE4rJjjp6pS+EvggG8ZIaSB6EeCyTqkWgmahODnjOEz8sBRdLIlrR8VpnoB1nAz4/GZXKo6K7HlyLLyDTOfcGQiigW0bvrSKi2YfdlekxJalMpHdsX1Hi5SSIUSPOAykD7FHVWt2Q18ctItOTrXLcKXxYDp6rXH+zmD/u+fKg/H8wed+AbWFVYFlEB6e0qssuzpqm6NeHMAlQELnJZUiFieanZq+D5H9Tp6jOxjBUA3au3rP9wJBTCYFiDIBiuG+0RkX2zk7PR jindrich.skupa@gmail.com
            jindrich.skupa:ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJDFRcIrAV4pXafNkXIu81Ouq1AwLFb9+o9NBzqaW0QU jindrich.skupa@gmail.com
            vault:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC5IF8pghENqvqxZkrf7gWczsLp1yh5DAmZ/0vSMDFpdNtKu5fvvK3mTkR8msmZdu7Dpd2APyLtCD57DBFzes5Q2R0IM1DbGEMOMzE3WLkr+slLHT3Ziafd+fpAeNRXNss2euc16Jw5RM38wBsPew0WEdhtw+MtJLGwXpqgeloTky8zxdeFlQKmBhnjft5jMlf+i00s+9uN13oTH6R7BVVORyd3jHks81Xe3E7O8tqGb0gNr73BuZSvcEuYJt7s3lJOLIKONYhd492LFndI2K4TLr5zFWFi+ovceQ1/o/s/5gC0a1Ozj6+vgIobByad+tCSy8e4H8ZlkjoLfJl9zLc4yd3UABEkvdYghFHrgNffdLjdFXefiQp5tt+ssfuc0OcP3EWs6B/AAQ+xqwXfoRMls/3f5LAWvAY/BdPsBT5xBv0osi8D6Z3iN6ypCniCMncgU2o3brxHTZr2LwTEt/+M54B5fv4AAIjhc7zpDLQzgNGMsuhcpi8MuokiGepRCAs= vault@example.com
        EOT
  }
}
