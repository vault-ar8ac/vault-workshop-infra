output "dns_zone" {
  value = var.dns_zone
}
output "name_servers" {
  value = google_dns_managed_zone.base.name_servers
}

output "instances" {
  value = [for server in var.instances : { instance = server, name = module.machines[server].name, public_ip = module.machines[server].public_ip, dns = google_dns_record_set.machines[server].name }]
}
