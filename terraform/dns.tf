locals {
  domain_slug = replace(replace(replace(lower(var.dns_zone), "/\\W|_|\\s/", "-"), "/-+/", "-"), "/-$/", "")
}
resource "google_dns_managed_zone" "base" {
  name        = "dns-${local.domain_slug}"
  dns_name    = var.dns_zone
  description = "Vault workshop sgfl.xyz DNS zone"
  labels = {
    domain = "sgfl"
  }
}

resource "google_dns_record_set" "machines" {
  for_each = toset(var.instances)
  name     = "${each.key}-${var.environment}.${google_dns_managed_zone.base.dns_name}"
  type     = "A"
  ttl      = 300

  managed_zone = google_dns_managed_zone.base.name

  rrdatas = [module.machines[each.key].public_ip]
}

