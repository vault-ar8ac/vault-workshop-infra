#!/bin/bash

GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

inventory=${1:-'hosts'}
playbook=$2

[ -d ~/.ssh ] || mkdir ~/.ssh

eval "$(ssh-agent -s)"
cat $CI_PROJECT_DIR/terraform/id_rsa_vault | ssh-add -

cat << EOF > ~/.ssh/config
Host *
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
    ForwardAgent yes
EOF

echo -e "============================================="
echo -e "Playbook: ${GREEN}${playbook}${NC}"
echo -e "============================================="

ansible-playbook -i ${inventory} ${playbook}.yml
